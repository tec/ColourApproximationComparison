module ColourApproximations

using Colors
using GLMakie
using Observables

# Utilities

include("coloursets.jl")
include("pairsurrounding.jl")

const coloursets =
    [Symbol("3-bit") => colours3bit,
     Symbol("4-bit") => colours4bit,
     Symbol("8-bit") => colours8bit,
     Symbol("24-bit") => Colorant[]]

# Extra colour differences

struct DE_RGB <: Colors.EuclideanDifferenceMetric{RGB} end

struct DE_HSL <: Colors.EuclideanDifferenceMetric{HSL} end

colour_distance_metrics =
    ["CIEΔE2000" => DE_2000(),
     "CIEΔE94" => DE_94(),
     "BFD" => DE_BFD(),
     "CMC" => DE_CMC(),
     # "JPC79" => DE_JPC79(),
     "l2 LAB" => DE_AB(),
     "l2 DIN99" => DE_DIN99(),
     # "l2 DIN99d" => DE_DIN99d(),
     # "l2 DIN99o" => DE_DIN99o(),
     "l2 RGB" => DE_RGB(),
     "l2 HSL" => DE_HSL()]

# The control observables

saturation = Observable(1.0)

y_granularity = Observable(256)
x_granularity = Observable(128)

const x_grads = round.(Int, 2 .^(sort(vcat(6:10, (6:10) .+ log2(1.5)))))
const y_grads = round.(Int, 2 .^(sort(vcat(5:10, (5:9) .+ log2(1.5)))))

diffmetric = Observable{Colors.DifferenceMetric}(last(first(colour_distance_metrics)))

# Plot setup

fig = Figure()

ax = Axis(fig[2,1])
hidedecorations!(ax)
hidespines!(ax)

controlrow1 = fig[1,1] = GridLayout()

xgrad_slider = Slider(controlrow1[1,2], range=y_grads, startvalue=y_granularity[])
Label(controlrow1[1,1], text="Y divisions")
ygrad_slider = Slider(controlrow1[1,4], range=x_grads, startvalue=x_granularity[])
Label(controlrow1[1,3], text="X divisions")
metric_slider = Slider(controlrow1[1,6], range=1:length(colour_distance_metrics))
Label(controlrow1[1,5], text=@lift(colour_distance_metrics[$(metric_slider.value)] |> first))

connect!(y_granularity, xgrad_slider.value)
connect!(x_granularity, ygrad_slider.value)

controlrow2 = fig[3,1] = GridLayout()

saturation_slider = Slider(controlrow2[1,2], range = 0:0.01:1, startvalue=saturation[])
Label(controlrow2[1,1], text="Saturation")
colourset_slider = Slider(controlrow2[1,4], range=1:length(coloursets))
Label(controlrow2[1,3], text=@lift(coloursets[$(colourset_slider.value)] |> first |> string))

colsize!(controlrow2, 1, Auto(true, 0.0))
colsize!(controlrow2, 2, Auto(false, 0.8))
colsize!(controlrow2, 3, Auto(true, 0.0))
colsize!(controlrow2, 4, Makie.Fixed(100))

# Plotting

connect!(saturation, saturation_slider.value)

map!(diffmetric, metric_slider.value) do v
    colour_distance_metrics[v] |> last
end

colour_grid_unprocessed = map(y_granularity, x_granularity, saturation) do x, y, s
    [HSL(h, s, l)
     for h in range(0, 360, length=x),
         l in range(0, 1, length=y)]
end

colour_grid = Observable(Matrix{Colorant}(undef, 0, 0))
map!(colour_grid, colour_grid_unprocessed, diffmetric, colourset_slider.value) #=
=# do cmat, metric, cset
    if cset == length(coloursets)
        cmat
    else
        growcolours(metric, last(coloursets[cset]), cmat)
    end
end

# The main event

image!(ax, colour_grid)

on(y_granularity, priority=-1) do _
    reset_limits!(ax)
end

on(x_granularity, priority=-1) do _
    reset_limits!(ax)
end

end
