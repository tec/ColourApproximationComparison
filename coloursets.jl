using Colors
using FixedPointNumbers

const colours3bit = # xterm colours
    [colorant"rgb(0, 0, 0)",
     colorant"rgb(205, 0, 0)",
     colorant"rgb(0, 205, 0)",
     colorant"rgb(205, 205, 0)",
     colorant"rgb(0, 0, 238)",
     colorant"rgb(205, 0, 205)",
     colorant"rgb(0, 205, 205)",
     colorant"rgb(229, 229, 229)"]

const colours4bit = # xterm again
    vcat(colours3bit,
         [colorant"rgb(127, 127, 127)",
          colorant"rgb(255, 0, 0)",
          colorant"rgb(0, 252, 0)",
          colorant"rgb(255, 255, 0)",
          colorant"rgb(0, 0, 252)",
          colorant"rgb(255, 0, 255)",
          colorant"rgb(0, 255, 255)",
          colorant"rgb(255, 255, 255)"])

const colour6cube =
    [RGB{N0f8}(r, g, b)
     for r in range(0, 1, length=6)
         for g in range(0, 1, length=6)
             for b in range(0, 1, length=6)]

const colour24greys = [RGB{N0f8}(w, w, w) for w in range(0, 1, length=24)]

const colours8bit = vcat(colours4bit, colour6cube, colour24greys)
